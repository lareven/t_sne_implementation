# This file intends to implement the t-SNE method
# for data visualization by Maaten and Hinton
# https://lvdmaaten.github.io/tsne/

import numpy as np

# let's start from SNE
def get_p_ji(x, i, j, sigmai):
    '''
    x is a high-dimensional dataset [m,...]
    output: conditional probability
    '''
    if i == j:
        return 0
    else:
        xi = x[i,:]
        xj = x[j,:]
        m = x.shape[0]
        numerator = np.exp(-np.linalg.norm(xi - xj)**2/(2*sigmai**2))

        denominator = 0
        for k in range(m):
            if k != i:
                denominator += np.exp(-np.linalg.norm(xi - x[k,:])**2/(2*sigmai**2))

        return numerator/denominator


def get_q_ji(y, i, j):
    sigma_y = 1/np.sqrt(2)
    return get_p_ji(y, i, j, sigma_y)


def compute_cost(x, y, sigma):
    m = x.shape[0]
    cost = 0

    for i in range(m):
        for j in range(m):
            if i != j:
                pji = get_p_ji(x, i, j, sigma[i])
                qji = get_q_ji(y, i, j)
                cost += pji * np.log(pji/qji)

    return cost



x = np.array([[[1,1,1],[2,2,2],[3,3,3]],\
    [[1,2,3],[4,5,6],[7,8,9]],\
    [[1,3,5],[2,4,6],[3,6,9]]])

y = np.array([[1,4], [5,2],[9,1]])

sigma = np.array([1,2,3])

#print(get_q_ji(y, 0,1))

print(compute_cost(x, y, sigma))

